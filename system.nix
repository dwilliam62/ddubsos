{ inputs, config, pkgs,
  username, hostname, ... }:

let 
  inherit (import ./options.nix) 
    theLocale theTimezone gitUsername
    theShell wallpaperDir wallpaperGit
    theLCVariables theKBDLayout flakeDir
    theme Modkey steam;
in {
  imports =
    [
      ./hardware.nix
      ./config/system
    ];

  # Enable networking
  networking.hostName = "${hostname}"; # Define your hostname
  networking.networkmanager.enable = true;
  networking.firewall.enable = false;
  networking.firewall.trustedInterfaces = [ "incusbr0"];

  networking.extraHosts =
  ''
  192.168.40.11         nas
  192.168.40.10         docker
  192.168.40.60         pbs
  192.168.40.217        porthos
  192.168.40.214        macbook
  192.168.40.212        asus
  192.168.40.195        ixas
  192.168.40.11         ds1817
  192.168.40.11         ds1817-server
  192.168.40.243        bubo
  192.168.40.52         bubo-wifi
  192.168.40.221        pve2
  192.168.40.9          pve3
  192.168.40.4          pbs2
  192.168.40.38         m6600
  192.168.40.65         pihole
  192.168.40.5          dellprinter
  192.168.40.8          mightymini
  192.168.40.30         donphone
  192.168.40.2          google-nest-hub
  192.168.40.1          router
  192.168.40.1          gateway
  192.168.40.65         dns1
  1.1.1.1               dns2
  8.8.4.4               dns3
  192.168.40.6          workpc
  192.168.40.4          pixel
  '';


  networking.nameservers = [ "1.1.1.1" "8.8.4.4"];
  networking.networkmanager.dns = "none";


  # Set your time zone
  time.timeZone = "${theTimezone}";

  # Select internationalisation properties
  i18n.defaultLocale = "${theLocale}";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "${theLCVariables}";
    LC_IDENTIFICATION = "${theLCVariables}";
    LC_MEASUREMENT = "${theLCVariables}";
    LC_MONETARY = "${theLCVariables}";
    LC_NAME = "${theLCVariables}";
    LC_NUMERIC = "${theLCVariables}";
    LC_PAPER = "${theLCVariables}";
    LC_TELEPHONE = "${theLCVariables}";
    LC_TIME = "${theLCVariables}";
  };

  console.keyMap = "${theKBDLayout}";

  # Define a user account.
  users = {
    mutableUsers = true;
    users."${username}" = {
      homeMode = "755";
      hashedPassword = "$6$YdPBODxytqUWXCYL$AHW1U9C6Qqkf6PZJI54jxFcPVm2sm/XWq3Z1qa94PFYz0FF.za9gl5WZL/z/g4nFLQ94SSEzMg5GMzMjJ6Vd7.";
      isNormalUser = true;
      description = "${gitUsername}";
      extraGroups = [ "networkmanager" "wheel" "libvirtd" ];
      shell = pkgs.${theShell};
      ignoreShellProgramCheck = true;
      packages = with pkgs; [];
    };
  };

  environment.variables = {
    FLAKE = "${flakeDir}";
    POLKIT_BIN = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
  };
 
  programs.nano.enable=false;

  # Optimization settings and garbage collection automation
  nix = {
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
      substituters = ["https://hyprland.cachix.org"];
      trusted-public-keys = [
        "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
      ];
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };
security.sudo = {
       enable = true;
       extraRules = [
         {
            users = [ "dwilliams" ];
            commands = [
              {
                 command = "ALL";
                 options = [ "NOPASSWD" ];
              }
           ];
         }
       ];
     };



  hardware.graphics = {
      enable = true;
    extraPackages = with pkgs; [ mesa ];
};


 system.stateVersion = "23.11";
}
