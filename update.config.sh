#!/usr/bin/env bash

# Get the hostname
hostname=$(hostname)

# Define the source directory
source_dir=~/ddubsos/hosts/"$hostname"

# Define destination directory (assuming same for both files)
dest_dir=~/ddubsos

# Define file names
files=("options.nix" "hardware.nix")

# Loop through each file and copy
for file in "${files[@]}"; do
  source_path="$source_dir/$file"
  dest_path="$dest_dir/$file"

  # Check if source file exists
  if [ ! -f "$source_path" ]; then
    echo "Error: File '$source_path' does not exist."
    continue  # Skip to the next iteration if this file doesn't exist
  fi

  # Copy the file
  cp "$source_path" "$dest_path"

  if [ $? -eq 0 ]; then
    echo "File '$source_path' copied to '$dest_path' successfully."
  else
    echo "Error: Failed to copy '$source_path'."
  fi
done

