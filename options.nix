# PLEASE READ THE WIKI FOR DETERMINING
# WHAT TO PUT HERE AS OPTIONS. 
# https://gitlab.com/Zaney/zaneyos/-/wikis/Setting-Options

let
  # THINGS YOU NEED TO CHANGE
  username = "dwilliams";
  hostname = "ddubsos-dev";
  userHome = "/home/${username}";
  flakeDir = "${userHome}/ddubsos";
  waybarStyle = "default"; # simplebar, slickbar, or default
in {

  # do not edit 
  slickbar = if waybarStyle == "slickbar" then true else false;
  simplebar = if waybarStyle == "simplebar" then true else false;

  # User Variables
  username = "${username}";
  hostname = "${hostname}";
  gitUsername = "Don Williams";
  gitEmail = "don.e.williams@gmail.com";

  # Set default theme for system
  theme = "dracula";

  # Enable / Disable Workspace Numbers In Waybar
  bar-number = true;
  waybarAnim = false;
  borderAnim = false;

  # Set default browswer
  browser = "google-chrome";

  # ^ (use as is or replace with your own repo - removing will break the wallsetter script) 
  # This will give you zaneys wallpapers
  wallpaperGit = "https://gitlab.com/Zaney/my-wallpapers.git";
  wallpaperDir = "${userHome}/Pictures/Wallpapers";

  screenshotDir = "${userHome}/Pictures/Screenshots";

  flakeDir = "${flakeDir}";
  flakePrev = "${userHome}/.ddubsos-previous";
  flakeBackup = "${userHome}/.ddubsos-backup";

  # Set default term for hyprland
  terminal = "kitty";

  # System Settings
  clock24h = false;
  theLocale = "en_US.UTF-8";

  # Keyboard settings 
  theKBDLayout = "us";
  theSecondKBDLayout = "de";
  theKBDVariant = "";

  theLCVariables = "en_US.UTF-8";
  theTimezone = "America/New_York";

  # Possible options: bash, zsh
  theShell = "zsh";

  # Possible options: default, latest, lqx, xanmod, zen
  theKernel = "latest";

  # Either x11 or wayland ONLY. Games might require x11 set here
  sdl-videodriver = "wayland";

  # For Hybrid Systems intel-nvidia
  # Should Be Used As gpuType
  # valid options are intel, amd, intel-nvidia and vm 
  # Lower case ONLY
  cpuType = "vm";
  gpuType = "vm";

 
  #SDDM backend   // Have to disable wayland on bubo laptop
 sddm-wayland=true; 

  # Set Hyprland Modifier for vm vs bare metal
  ModKey = "SUPER";

  # Nvidia Hybrid Devices
  # ONLY NEEDED FOR HYBRID
  # SYSTEMS! 
  intel-bus-id = "PCI:0:2:0";
  nvidia-bus-id = "PCI:14:0:0";

  # Enable / Setup NFS
  nfs = {
    enable = true;
    nfsMountPoint = "/mnt/nas";
    nfsDevice = "nas:/volume1/DiskStation54TB";
    options = [ "bg" "soft" "tcp" "_netdev" ];
  };

  # Enable Printer & Scanner Support
  printer = false;

  # Program#s
  distrobox = false;
  flatpak = true;
  kdenlive = false;
  blender = false;
  quickemu = false;
  vscode = false;
  libreoffice = false;
  freeoffice = false;
  steam = false;
  python = true;
  syncthing = false;
  element = false;
  obs = false;

  # ChatGPT for all 
  gpt4all = false;
  # video offload intel or amd
  offload = "amd";

  # If You Disable All You Get Kitty
  wezterm = false;
  alacritty = true;
  kitty = true;
  fish = true;
  warp = true;

  # Awesome window mgr
  awesome = false;

  # Logitech Devices
  logitech = false;

  # NTP & HWClock Settings
  ntp = true;
  localHWClock = true;
}
