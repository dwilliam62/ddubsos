#!/usr/bin/env bash

pgrep -x "wf-recorder" && pkill -INT -x wf-recorder && notify-send -h string:wf-recorder:record -t 1000 "Finished Recording" && exit 0

wf-recorder -a -g "$(slurp)" -f "$VID" &>/dev/null

dateTime=$(date +%m-%d-%Y-%H:%M:%S)
wf-recorder -a -g "$(slurp)" --bframes max_b_frames -f $HOME/Videos/$dateTime.mp4


