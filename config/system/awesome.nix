{ pkgs, config, lib, ... }:

let inherit (import ../../options.nix) awesome;

in lib.mkIf (awesome == true) {

   
services.xserver.windowManager.awesome = {
      enable = true;
      luaModules = with pkgs.luaPackages; [
        luarocks # is the package manager for Lua modules
        luadbi-mysql # Database abstraction layer
      ];
    };

 environment.systemPackages = [
    pkgs.awesome
  ];
  
}

