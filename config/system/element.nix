{ config, pkgs, lib, ... }:

let inherit (import ../../options.nix) element;

in lib.mkIf (element == true) {
  environment.systemPackages = with pkgs; [
    pkgs.element-desktop
  ];
}

