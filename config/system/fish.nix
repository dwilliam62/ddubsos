{ config, pkgs, lib, ... }:

let inherit (import ../../options.nix) fish;

in lib.mkIf (fish == true) {

environment.systemPackages = with pkgs; [
  fishPlugins.done
  fishPlugins.fzf-fish
  fishPlugins.forgit
  fishPlugins.hydro
  fishPlugins.tide
  fzf
  fishPlugins.grc
  grc
];
 programs.fish.enable = true;

}

