{ config, pkgs, lib, ... }:

let inherit (import ../../options.nix) obs;

in lib.mkIf (obs == true) {
  environment.systemPackages = with pkgs; [
    pkgs.obs-studio
  ];
}

