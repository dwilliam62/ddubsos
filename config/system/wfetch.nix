{ pkgs, config, lib, ... }:

{
  inputs.wfetch = {
    url = "github:iynaix/wfetch";
    inputs.nixpkgs.follows = "nixpkgs"; # override this repo's nixpkgs snapshot
  };
}

