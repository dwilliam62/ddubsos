{ pkgs, config, lib, inputs, ... }:

let inherit (import ../../options.nix) gpt4all;

in lib.mkIf (gpt4all == true ) {

  # Set environment variables for gpt4all
     environment.variables = {
       GPT4ALL_GPU_DEVICE = "amd";
       GPT4ALL_OFFLOADING = "true";
   };

  # {
  # Set environment variables for gpt4all for intel
  #environment.variables = {
  #  GPT4ALL_OPENCL_DRIVER = "intel";
  #  GPT4ALL_OFFLOADING = "true";
  #};

}
