{ pkgs, config, lib, inputs, ... }:

let inherit (import ../../options.nix) cpuType;

in lib.mkIf (cpuType != "vm") {

  services.smartd = {
      enable = true;
      autodetect = true;
  };
}
