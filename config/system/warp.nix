{ config, pkgs, lib, ... }:

let inherit (import ../../options.nix) warp;

in lib.mkIf (warp == true) {

environment.systemPackages = with pkgs; [
  warp-terminal
];

}

