{ pkgs, config, inputs, ... }:

{
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List System Programs
  environment.systemPackages = with pkgs; [
    wget
    curl
    git
    cmatrix
    lolcat
    neofetch
    htop
    libvirt
    polkit_gnome
    lm_sensors
    unzip
    unrar
    libnotify
    eza
    v4l-utils
    ydotool
    wl-clipboard
    socat
    cowsay
    lsd
    lshw
    pkg-config
    hugo
    #symbola
    noto-fonts-color-emoji
    material-icons
    brightnessctl
    toybox
    virt-viewer
    swappy
    appimage-run
    networkmanagerapplet
    yad
    playerctl
    nh

    # My packages

    # Misc Utils
    fastfetch
    rsync
    #dex
    ntfs3g
    p7zip
    ouch
    gearlever

    # Internet
    google-chrome
    dig
    ncftp
    gping
    ntp
    putty
    remmina
    vesktop
    (inputs.wezterm.packages.${pkgs.system}.default)

    # CLI utils
    mc
    ncdu
    fzf
    sshfs
    zoxide
    most
    dua
    figlet
    cava
    tmux
    zellij

    # Monitoring
    atop
    gtop
    gotop
    glances
    bottom
    stacer
    mission-center
    iotop
    cpuid
    powertop
    cpu-x
    smartmontools
    gsmartcontrol
    ipfetch
    hyfetch
    inxi
    nvtopPackages.full
    cpufetch
    pfetch
    hyfetch

    # Programming utis
    nodejs
    go
    ninja
    meson
    ripgrep
    ugrep
    gnumake
    #delta
    lazygit
    nixfmt-classic
    gcc
    cmake
    fd
    meld
    clang-tools
    zig
    foot
    warp-terminal

    # Media / Video 
    vlc
    okular
    jellyfin-media-player
    nomacs

    #hyprland related


#  override for aquamarine 
   (aquamarine.overrideAttrs (oldAttrs: {
       inherit (oldAttrs) pname;
     version = "0.4.4";
    }))

   #   override for hyprland 
   (hyprland.overrideAttrs (oldAttrs: {
      inherit (oldAttrs) pname;
      version = "0.44.0";
    }))
    

    wpaperd
    hyprpaper
    nwg-drawer
    nwg-launchers
    nwg-panel
    wf-recorder
    x264
    faac
    faad2
    hyprwayland-scanner
    hyprlang
    hyprutils
    hyprshot
        #aquamarine
    pywal
    wallust
    waypaper

    # File MGr
    yazi
    duf

    # Editors
    neovim
    neovide

   #  Local free A.I. engine
  # gpt4all
   #intel-ocl

  ];

  programs = {
    steam.gamescopeSession.enable = false;
    dconf.enable = true;
    seahorse.enable = true;
    hyprland = {
      enable = true;
      package = inputs.hyprland.packages.${pkgs.system}.hyprland;

    # set the flake package
    #package = hyprland.packages.${pkgs.stdenv.hostPlatform.system}.hyprland;
    # make sure to also set the portal package, so that they are in sync
    #portalPackage = hyprland.packages.${pkgs.stdenv.hostPlatform.system}.xdg-desktop-portal-hyprland;

      xwayland.enable = true;
    };
    fuse.userAllowOther = true;
    mtr.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    virt-manager.enable = true;
  };

  virtualisation.libvirtd.enable = true;
}
