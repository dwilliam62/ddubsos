{ pkgs, config, lib, ... }:

let inherit (import ../../options.nix) freeoffice;

in lib.mkIf (freeoffice == true) {
  environment.systemPackages = with pkgs;
    [

      pkgs.freeoffice

    ];

}

