{ pkgs, config, lib, ... }:

let inherit (import ../../options.nix) quickemu;

in lib.mkIf (quickemu == true) {

  virtualisation = {
    libvirtd = {
      enable = true;
      qemu = {
        swtpm.enable = true;
        ovmf = {
          enable = true;
          packages = [ pkgs.OVMFFull.fd ];
        };
      };
    };
  };


 # Incus LXC Containers
  virtualisation.incus = {
    enable = false;
    ui.enable = false;
    };

  environment.systemPackages = with pkgs; [

    pkgs.quickemu
   # pkgs.quickgui  ## Fails to build
    pkgs.incus


  ];

}

