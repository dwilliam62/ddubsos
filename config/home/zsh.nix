{ config, lib, pkgs, ... }:

let inherit (import ../../options.nix) flakeDir theShell hostname;
in lib.mkIf (theShell == "zsh") {
  programs.zsh = {
    enable = true;
    syntaxHighlighting.enable = true;
    autosuggestion.enable = true;
    #enableAutosuggestions = true;
    historySubstringSearch.enable = true;
    profileExtra = ''
      #if [ -z "$DISPLAY" ] && [ "$XDG_VNTR" = 1 ]; then
      #  exec Hyprland
      #fi
    '';
    initExtra = ''
      zstyle ":completion:*" menu select
      zstyle ":completion:*" matcher-list "" "m:{a-z0A-Z}={A-Za-z}" "r:|=*" "l:|=* r:|=*"
      if type nproc &>/dev/null; then
        export MAKEFLAGS="$MAKEFLAGS -j$(($(nproc)-1))"
      fi
      bindkey '^[[3~' delete-char                     # Key Del
      bindkey '^[[5~' beginning-of-buffer-or-history  # Key Page Up
      bindkey '^[[6~' end-of-buffer-or-history        # Key Page Down
      bindkey '^[[1;3D' backward-word                 # Key Alt + Left
      bindkey '^[[1;3C' forward-word                  # Key Alt + Right
      bindkey '^[[H' beginning-of-line                # Key Home
      bindkey '^[[F' end-of-line                      # Key End
     # neofetch
     fastfetch
      if [ -f $HOME/.zshrc-personal ]; then
        source $HOME/.zshrc-personal
      fi
      eval "$(starship init zsh)"
    '';
    initExtraFirst = ''
      HISTFILE=~/.histfile
      HISTSIZE=1000
      SAVEHIST=1000
      setopt autocd nomatch
      unsetopt beep extendedglob notify
      autoload -Uz compinit
      compinit
    '';
    sessionVariables = {

    };

    oh-my-zsh = {
      enable = true;
      plugins = [ "git" "npm" "history" "node" "rust" "deno" ];
      theme = "robbyrussell";
    };

    shellAliases = {
      sv = "sudo nvim";
      restart = "systemctl reboot";
      rebuild = "nh os switch  --hostname ${hostname} -- --cores=4";
      flake-rebuild = "nh os switch --hostname ${hostname} -- --cores=4";
      flake-update = "nh os switch --hostname ${hostname} --update -- --cores=4";
      update = "nh os switch --hostname ${hostname} --update -- --cores=4";
      gcCleanup =
        "nix-collect-garbage --delete-old && sudo nix-collect-garbage -d && sudo /run/current-system/bin/switch-to-configuration boot";
      v = "nvim";
      vi = "nvim";
      vim = "nvim";
      nv = "neovide";
      lv = "nvim -u ~/.config/nvim.leaf/init.vim";
      lvd = "neovide -- -u ~/.config/nvim.leaf/init.vim";
      poweroff = "systemctl poweroff";
      genhw = "nixos-generate-config --show-hardware-config >~/ddubsos/hardware.nix";
      ls = "lsd";
      ll = "lsd -l";
      la = "lsd -a";
      lal = "lsd -al";
      ".." = "cd ..";
      "..." = "cd ../..";
      h = "history | grep ";
      hs = "history | grep ";
      f = "find . | grep ";
      tree = "tree -CAhf --dirsfirst ";
      treed = "tree -CAFd";
      c = "clear";
      j = "jobs -l";
      gp = "gping -c red -b 360 1.1.1.1";
      bat = "bat --style header snip --style changes --style header";
      neofetch = "neofetch --ascii ~/.config/ascii-neofetch";
    };
  };

}
