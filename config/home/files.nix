{ pkgs, config, ... }:

{
  # Place Files Inside Home Directory
  home.file.".emoji".source = ./files/emoji;
  home.file.".base16-themes".source = ./files/base16-themes;
  home.file.".face".source = ./files/face.jpg; # For GDM
  home.file.".face.icon".source = ./files/face.jpg; # For SDDM
  home.file.".config/rofi/rofi.jpg".source = ./files/rofi.jpg;
  home.file.".config/starship.toml".source = ./files/starship.toml;
  home.file.".config/swaylock-bg.jpg".source = ./files/media/swaylock-bg.jpg;
  home.file.".config/ascii-neofetch".source = ./files/ascii-neofetch;
  #
  # My configs
  #
  home.file.".bashrc-personal".source = ./files/.bashrc-personal;
  home.file.".zshrc-personal".source = ./files/.zshrc-personal;
  home.file.".config/neofetch/config.conf".source = ./files/neofetch.config.conf.ddubsos;
  home.file.".config/hypr/WaybarCava.sh".source = ./files/WaybarCava.sh;
  home.file.".config/fastfetch/nixos.png".source = ./files/nixos.png;
  home.file.".config/wpaperd/wallpaper.toml".text = ''
    [default]
       path = "/home/dwilliams/Pictures/Wallpapers"
       duration = "120m"
       sorting = "random"
  '';

  #Fastfetch config
  home.file.".config/fastfetch/config.jsonc".text = ''
              {
                  "$schema": "https://github.com/fastfetch-cli/fastfetch/raw/dev/doc/json_schema.json",
                   "logo": {
                      "source": "~/.config/fastfetch/nixos.png",
                      "type": "kitty-direct",
                      "height": 15,
                      "width": 30,
                   "padding": {
                    "top": 2
             }
         },
          "display": {
               "separator": " ➜  "
         },
          "modules": [
                "break",
                "break",
                "break",
           {
              "type": "os",
              "key": "OS (ddubsos 0.8.9)  ",
              "keyColor": "31",  // = color1
          },
          {
              "type": "kernel",
              "key": " ├  ",
              "keyColor": "31",
          },
          {
              "type": "packages",
              "format": "{} (NIXOS)",
              "key": " ├ 󰏖 ",
              "keyColor": "31",  
          },
          {
              "type": "shell",
              "key": " └  ",
              "keyColor": "31", 
          },
          "break",
          {
              "type": "wm",
              "key": "WM   ",
              "keyColor": "32", 
          },
          {
              "type": "wmtheme",
              "key": " ├ 󰉼 ",
              "keyColor": "32",
          },
          {
              "type": "icons",
              "key": " ├ 󰀻 ",
              "keyColor": "32",
          },
          {
              "type": "cursor",
              "key": " ├  ",
              "keyColor": "32", 
          },
          {
              "type": "terminal",
              "key": " ├  ",
              "keyColor": "32",
          },
          {
              "type": "terminalfont",
              "key": " └  ",
              "keyColor": "32", 
          },
          "break",
          {
              "type": "host",
              "format": "{5} {1} Type {2}",
              "key": "PC   ",
              "keyColor": "33",
          },
          {
              "type": "cpu",
              "format": "{1} ({3}) @ {7} GHz",
              "key": " ├  ",
              "keyColor": "33",
          },
          {
              "type": "gpu",
              "format": "{1} {2} @ {12} GHz",            
              "key": " ├ 󰢮 ",
              "keyColor": "33",
          },
          {
              "type": "memory",
              "key": " ├  ",
              "keyColor": "33",
          },
          {
              "type": "swap",
              "key": " ├ 󰓡 ",
              "keyColor": "33",
          },
          {
              "type": "disk",
              "key": " ├ 󰋊 ",
              "keyColor": "33",
          },
          {
              "type": "monitor",
              "key": " └  ",
              "keyColor": "33",
          },
          "break",
          "break",
      ]
    }
  '';


  home.file.".config/tmux" = {
    source = ./files/tmux;
    recursive = true;
  };

  home.file.".config/mc" = {
    source = ./files/mc;
    recursive = true;
  };

#  home.file.".config/btop/themes" = {
#.   source = ./files/btop/themes;
#    recursive = true;
#  };

  home.file.".config/putty" = {
    source = ./files/putty;
    recursive = true;
  };

  home.file.".local/share/fonts" = {
    source = ./files/fonts;
    recursive = true;
  };
  home.file.".config/wlogout/icons" = {
    source = ./files/wlogout;
    recursive = true;
  };
  home.file.".config/obs-studio" = {
    source = ./files/obs-studio;
    recursive = true;
  };

  home.file.".config/lazygit/config.yaml".text = ''
    gui:
      authorColors:
        'Abhishek Keshri': green
        # stuff relating to the UI
      branchColors:
        main: green
        master: green
        docs: cyan
        feature: yellow
        staging: magenta
      scrollHeight: 2 # how many lines you scroll by
      scrollPastBottom: true # enable scrolling past the bottom
      sidePanelWidth: 0.25 # number from 0 to 1
      expandFocusedSidePanel: true
      mainPanelSplitMode: 'flexible' # one of 'horizontal' | 'flexible' | 'vertical'
      language: 'auto' # one of 'auto' | 'en' | 'zh' | 'pl' | 'nl' | 'ja' | 'ko'
      timeFormat: '02 Jan 06 15:04 MST' # https://pkg.go.dev/time#Time.Format
      theme:
        activeBorderColor:
          - blue
          - bold
        inactiveBorderColor:
          - default
        searchingActiveBorderColor:
          - green
          - bold
        optionsTextColor:
          - blue
        selectedLineBgColor:
          - blue # set to `default` to have no background colour
        selectedRangeBgColor:
          - blue
        cherryPickedCommitBgColor:
          - cyan
        cherryPickedCommitFgColor:
          - blue
        defaultFgColor:
          - white
      commitLength:
        show: true
      mouseEvents: true
      skipDiscardChangeWarning: false
      skipStashWarning: false
      showFileTree: true # for rendering changes files in a tree format
      showListFooter: true # for seeing the '5 of 20' message in list panels
      showRandomTip: true
      showBottomLine: true # for hiding the bottom information line (unless it has important information to tell you)
      showCommandLog: true
      # showIcons: true
      nerdFontsVersion: '3' # nerd fonts version to use ("2" or "3"); empty means don't show nerd font icons
      commandLogSize: 8
      splitDiff: 'auto' # one of 'auto' | 'always'
    os:
      editPreset: nvim
    git:
      paging:
        colorArg: always
        useConfig: false
        pager: delta --features=mantis-shrimp --paging=never
      commit:
        signOff: false
      merging:
        # only applicable to unix users
        manualCommit: false
        # extra args passed to `git merge`, e.g. --no-ff
        args: '  ' 
      log:
        # one of date-order, author-date-order, topo-order.
        # topo-order makes it easier to read the git log graph, but commits may not
        # appear chronologically. See https://git-scm.com/docs/git-log#_commit_ordering
        order: 'topo-order'
        # one of always, never, when-maximised
        # this determines whether the git graph is rendered in the commits panel
        showGraph: 'when-maximised'
        # displays the whole git graph by default in the commits panel (equivalent to passing the `--all` argument to `git log`)
        showWholeGraph: false
      skipHookPrefix: WIP
      autoFetch: true
      autoRefresh: true
      branchLogCmd: 'git log --graph --color=always --abbrev-commit --decorate --date=relative --pretty=medium {{branchName}} --'
      allBranchesLogCmd: 'git log --graph --all --color=always --abbrev-commit --decorate --date=relative  --pretty=medium'
      overrideGpg: false # prevents lazygit from spawning a separate process when using GPG
      disableForcePushing: false
      parseEmoji: true
    refresher:
      refreshInterval: 10 # File/submodule refresh interval in seconds. Auto-refresh can be disabled via option 'git.autoRefresh'.
      fetchInterval: 600 # Re-fetch interval in seconds. Auto-fetch can be disabled via option 'git.autoFetch'.
    update:
      method: prompt # can be: prompt | background | never
      days: 14 # how often an update is checked for
    confirmOnQuit: false
    # determines whether hitting 'esc' will quit the application when there is nothing to cancel/close
    quitOnTopLevelReturn: false
    disableStartupPopups: false
    notARepository: 'prompt' # one of: 'prompt' | 'create' | 'skip'
    promptToReturnFromSubprocess: true # display confirmation when subprocess terminates
  '';

}

