{ pkgs, config, lib, ... }:

let
  palette = config.colorScheme.palette;
  inherit (import ../../options.nix) wezterm;
in lib.mkIf (wezterm == true) {
  home.packages = with pkgs; [  ];

  home.file.".config/wezterm/wezterm.lua".text = ''
        local wezterm = require 'wezterm'
        local config = {}
        config.color_scheme = 'AdventureTime'
        config.enable_wayland = true 
        return config
  '';
}
