{ config, pkgs, ... }:

{

  programs.btop = {
    enable = true;
    package = pkgs.btop.override { 
      rocmSupport = true; 
      cudaSupport = true; 
      };
    settings = {
      color_theme = "catppuccin_mocha";
      vim_keys = true;
      rounded_corners = false;
      theme_background = true;
    };
  };
}
