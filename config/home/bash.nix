{ config, lib, pkgs, ... }:

let
  inherit (import ../../options.nix)
    flakeDir flakePrev hostname flakeBackup theShell;
in lib.mkIf (theShell == "bash") {
  # Configure Bash
  programs.bash = {
    enable = true;
    enableCompletion = true;
    profileExtra = ''
      #if [ -z "$DISPLAY" ] && [ "$XDG_VTNR" = 1 ]; then
      #  exec Hyprland
      #fi
    '';
    initExtra = ''
      #neofetch
      fastfetch
      if [ -f $HOME/.bashrc-personal ]; then
        source $HOME/.bashrc-personal
      fi
    '';
    sessionVariables = {
      ZANEYOS = true;
      FLAKEBACKUP = "${flakeBackup}";
      FLAKEPREV = "${flakePrev}";
      EDITOR = "nvim";
      TERMINAL = "alacritty";
      BROWSER = "google-chrome-stable";
    };
    shellAliases = {
      sv = "sudo nvim";
      flake-rebuild = "nh os switch  --hostname ${hostname}";
      rebuild = "nh os switch --hostname ${hostname}";
      flake-update = "nh os switch --hostname ${hostname} --update";
      update = "nh os switch --hostname ${hostname} --update";
      gcCleanup =
        "nix-collect-garbage --delete-old && sudo nix-collect-garbage -d && sudo /run/current-system/bin/switch-to-configuration boot";
      v = "nvim";
      vi = "nvim";
      vim = "nvim";
      lv = "nvim -u ~/.config/nvim.leaf/init.vim";
      lvd = "neovide -- -u ~/.config/nvim.leaf/init.vim";
      restart = "systemctl reboot";
      poweroff = "systemctl poweroff";
      vd = "neovide";
      ls = "lsd";
      ll = "lsd -l";
      la = "lsd -a";
      lal = "lsd -al";
      ".." = "cd ..";
      "..." = "cd ../..";
      h = "history | grep ";
      hs = "history | grep ";
      f = "find . | grep ";
      tree = "tree -CAhf --dirsfirst ";
      treed = "tree -CAFd";
      c = "clear";
      j = "jobs -l";
      gp = "gping -c red -b 360 1.1.1.1";
      cat = "bat --style header snip --style changes --style header";
      genhw =
        "nixos-generate-config --show-hardware-config >~/ddubsos/hardware.nix";
      neofetch = "neofetch --ascii ~/.config/ascii-neofetch";
    };
  };
}
